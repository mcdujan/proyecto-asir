<?php

class WorkerCommand extends CConsoleCommand
{
    public function actionConsumir()
    {
        $consumer = Yii::app()->beanstalk->getClient();
        $consumer->connect();
        $consumer->watch('default');

        while(true)
        {
            $job = $consumer->reserve();
            $datos = json_decode($job['body']);
            // Operacion que queramos hacer con el trabajo
            // que devuelva bool
            $result = true;

            if( $result )
            {
                echo "Trabajo realizado..." . PHP_EOL;
                echo $datos->payload . PHP_EOL;
                $consumer->delete($job['id']);
            }
            else
            {
                echo "Enterrando el trabajo..." . PHP_EOL;
                $consumer->bury($job['id']);
            }
        }

        $consumer->disconnect();
    }
}
