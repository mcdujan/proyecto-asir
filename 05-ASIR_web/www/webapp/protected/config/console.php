<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(

		// database settings are configured in database.php
		'db'=> YII_DEBUG ? require(dirname(__FILE__).'/database.php') : require(dirname(__FILE__) . '/database_produccion.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'beanstalk'=> array(
            'class'=>'application.extensions.yii-dbeanstalkd.Beanstalk',
            'servers'=>YII_DEBUG ? require(dirname(__FILE__).'/cola.php') : require(dirname(__FILE__) . '/cola_produccion.php'),
        ),
	),
);
