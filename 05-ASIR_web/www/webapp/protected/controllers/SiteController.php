<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{

        $this->render('index');
	}

    public function actionColas() {
        $client = Yii::app()->beanstalk->getClient();
        $client->connect();

        $client->useTube('default');

        //echo "Tube used: {$client->listTubeUsed()}.\n";
        //print_r($client->stats());

        $jobDetails = [
            'aplicacion'=> 'Accion Colas',
            'payload'=>'Tarea Enviada el: '.date('d-m-Y H:i:s'),
            'ip'=>$_SERVER['SERVER_ADDR'],
        ];
        $jobDetailString = json_encode($jobDetails);

        $prioridad = rand(0,100);
        $retraso = rand(0,20);


        $ret = $client->put(
            $prioridad, // priority
            $retraso, // do not wait, put in immediately
            90, // will run within n seconds
            $jobDetailString // job body
        );

        $client->disconnect();

        $this->render('cola', array('ret'=>$ret,
            'detalles'=>$jobDetailString,
            'retraso'=>$retraso,
            'prioridad'=>$prioridad
        ));

    }
    public function actionCacheDb() {
        $c = Yii::app()->cache;

        $q = 'SELECT * FROM employees LIMIT 3000';

        $cmd = Yii::app()->db->cache(60)->createCommand($q);
        $filas = $cmd->queryAll();

        $stats = Yii::app()->db->getStats();

        $this->render('cacheo', array('stats'=>$stats, 'resultado'=>$filas));

    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
