<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>


<h1>Bienvenidos a  <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Información del trabajo encolado:</p>
<ul>
<li>Añadido a la cola: <b><?php echo $detalles; ?></b></li>
<li>Valor de retorno: <b><?php echo $ret; ?></b></li>
<li>Añadido con prioridad: <b><?php echo $prioridad; ?></b></li>
<li>Añadido con retraso: <b><?php echo $retraso; ?> segundos</b></li>
</ul>
