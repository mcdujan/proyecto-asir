<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Bienvenidos a  <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Estás viendo esta página gracias al servidor: <b><?php echo Yii::app()->request->getHostInfo(); ?></b></p>

<p>Gracias por visitar nuestra web</p>

