<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Bienvenidos a  <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Información de las consultas:</p>
<ul>
<li>Número de consultas ejecutadas: <b><?php echo $stats[0]; ?></b></li>
<li>Tiempo ejecución de las consultas: <b><?php echo $stats[1]; ?> segundos</b></li>
<li>Total de filas devueltas: <b><?php echo count($resultado); ?></b></li>
</ul>

